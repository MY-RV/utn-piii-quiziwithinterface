﻿using QuizIWithInterface.App.Lib.Datasets;
using QuizIWithInterface.MYRV.Extensions;
using QuizIWithInterface.App.Lib.Types;
using QuizIWithInterface.App.Lib.Enums;
using QuizIWithInterface.App.Entities;
using System.Text.RegularExpressions;
using QuizIWithInterface.App.Lib;
using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.Json;
using System.Linq;
using System;

namespace QuizIWithInterface
{
    public partial class MainFrame : Form
    {

        private Client CurrentClient { get; set; }
        private readonly HashSet<Coverage> _coverages;
        private readonly HashSet<Addiction> _addictions;
        private readonly Dictionary<string, string> _validationErrors;
        private readonly ISubject<bool> _unsubscribe = new Subject<bool>();
        private readonly BehaviorSubject<Client> _formClient;
        private readonly BehaviorSubject<bool> _loading;
        #region ClientFields
        private readonly ISubject<string> _clientIdCard;
        private readonly ISubject<string> _clientName;
        private readonly ISubject<Gender> _clientGender;
        private readonly ISubject<DateTime> _clientBirthday;
        private readonly BehaviorSubject<SimpleTree> _clientProvince;
        private readonly BehaviorSubject<SimpleTree> _clientCanton;
        private readonly BehaviorSubject<SimpleTree> _clientDistrict;
        private readonly ISubject<string> _clientOtherSings;
        #endregion

        public MainFrame()
        {
            InitializeComponent();
            FormClosing += OnFormClosing;
            DpkFechaNacimiento.MaxDate = DateTime.Now;
            LvwCoverturas.ItemChecked += LvwCoverturas_SelectedIndexChanged;
            LvwAdicciones.ItemChecked += LvwAdicciones_SelectedIndexChanged;
            CbxProvincia.Items.AddRange(Provinces.Data.Select(p => p.Name).ToArray());
            TblClients.DataSource = Clients.RefreshedData;

            _validationErrors = new Dictionary<string, string>();
            _coverages = new HashSet<Coverage>();
            _addictions = new HashSet<Addiction>();
            ClearErrorDictionary();

            FrameTabControl.TabPages.Remove(RequestTab);
            FrameTabControl.SelectedIndexChanged += (object sender, EventArgs e) =>
            {
                if (FrameTabControl.SelectedIndex != 1)
                {
                    FrameTabControl.TabPages.Remove(RequestTab);
                    this.Height = 650;
                }
                else
                {
                    this.Height = 550;
                    InitListsViews();
                }
            };

            #region DeclaringBehaviorSubjects
            _loading = new BehaviorSubject<bool>(true);
            _clientName = new BehaviorSubject<string>(TxtNombre.Text);
            _clientIdCard = new BehaviorSubject<string>(TxtCedula.Text);
            _clientGender = new BehaviorSubject<Gender>(RdbMasculino.Checked ? Gender.Male : Gender.Female);
            _clientBirthday = new BehaviorSubject<DateTime>(DpkFechaNacimiento.Value);
            _clientOtherSings = new BehaviorSubject<string>(TxtSennas.Text);
            _clientProvince = new BehaviorSubject<SimpleTree>(null);
            _clientCanton = new BehaviorSubject<SimpleTree>(null);
            _clientDistrict = new BehaviorSubject<SimpleTree>(null);
            _formClient = new BehaviorSubject<Client>(null);
            #endregion

            InitSubscriptions();
            _loading.OnNext(false);
            DpkFechaNacimiento.Value = DpkFechaNacimiento.MaxDate;
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            _unsubscribe.OnNext(true);
        }

        private void InitSubscriptions()
        {
            #region ValidationSubscriptions
            _clientIdCard.TakeUntil(_unsubscribe).Subscribe((value) =>
            {
                LblErrCedula.Text = null;
                if (_loading.Value) return;
                if (Regex.Replace(value, @"[0-9a-zA-Z]", "").Length > 0)
                    LblErrCedula.Text = "Usar solo letras, espacios, números y \"-\"";
                else if (value.Length < 9)
                    LblErrCedula.Text = "La Cédula debe de tener almenos 9 digitos";
                else if (Clients.Data.FirstOrDefault(c => c.IdCard == value) != null) 
                    LblErrCedula.Text = "La Cédula ya esta en uso";
                else LblErrCedula.Text = null;
                _validationErrors["IdCard"] = LblErrCedula.Text;
            });
            _clientName.TakeUntil(_unsubscribe).Subscribe((value) =>
            {
                LblErrNombre.Text = null;
                value = value.Replace(" ", "");
                if (_loading.Value) return;
                if (Regex.Replace(value, @"[0-9a-zA-Z]", "").Length > 0)
                    LblErrNombre.Text = "Usar solo letras, espacios, números y \"-\"";
                else if (value.Length < 5)
                    LblErrNombre.Text = "Usar almenos 5 caracteres";
                else LblErrNombre.Text = null;
                _validationErrors["Name"] = LblErrNombre.Text;
            });
            _clientBirthday.TakeUntil(_unsubscribe).Subscribe((value) =>
            {
                TxtEdad.Text = value.GetRoundedAge().ToString();
                LblErrEdad.Text = null;
                if (_loading.Value) return;
                if (value.GetRoundedAge() < 18)
                    LblErrEdad.Text = "Debes de ser Mayor de Edad para registrate";
                else if (value.GetRoundedAge() > 50)
                    LblErrEdad.Text = "Debes de ser Menor de 50 años para registrate";
                else LblErrEdad.Text = null;
                _validationErrors["Birthday"] = LblErrEdad.Text;
            });

            _clientProvince.DistinctUntilChanged(
                p => p?.Name
            ).TakeUntil(_unsubscribe).Subscribe((value) =>
            {
                ReloadCantons(value);
                LblErrProvincia.Text = null;
                if (_loading.Value) return;
                if (value == null)
                    LblErrProvincia.Text = "Debes Seleccionar una Provincia";
                else LblErrProvincia.Text = null;
                _validationErrors["Province"] = LblErrCedula.Text;
            });
            _clientCanton.DistinctUntilChanged(
                c => $"{_clientProvince.Value}:{c?.Name}"
            ).TakeUntil(_unsubscribe).Subscribe((value) =>
            {
                ReloadDistricts(value);
                LblErrCanton.Text = null;
                if (_loading.Value) return;
                if (value == null)
                    LblErrCanton.Text = "Debes Seleccionar un Cantón";
                else LblErrCanton.Text = null;
                _validationErrors["Canton"] = LblErrCanton.Text;
            });
            _clientDistrict.DistinctUntilChanged(
                d => $"{_clientCanton.Value}:{d?.Name}"
            ).TakeUntil(_unsubscribe).Subscribe((value) =>
            {
                LblErrDistrito.Text = null;
                if (_loading.Value) return;
                if (value == null)
                    LblErrDistrito.Text = "Debes Seleccionar un Distrito";
                else LblErrDistrito.Text = null;
                _validationErrors["District"] = LblErrDistrito.Text;
            });
            #endregion

            #region ObservableUser
            Observable.CombineLatest(
                _clientIdCard, _clientName, _clientBirthday, _clientGender,
                _clientProvince.DistinctUntilChanged(p => p?.Name),
                _clientCanton.DistinctUntilChanged(c => c?.Name),
                _clientDistrict.DistinctUntilChanged(d => d?.Name),
                _clientOtherSings,
                (IdCard, Name, Birthday, Gender, Province, Canton, District, OtherSigns) => new Client
                {
                    IdCard = IdCard,
                    Name = Name,
                    Birthday = Birthday,
                    Gender = Gender,
                    Province = Province?.ToString(),
                    Canton = Canton?.ToString(),
                    District = District?.ToString(),
                    OtherSigns = OtherSigns,
                }
            ).TakeUntil(_unsubscribe).Subscribe(_formClient.OnNext);
            #endregion

            _formClient.TakeUntil(_unsubscribe).Subscribe((value) =>
            {
                #if DEBUG
                Debug.WriteLine("\r\nDEBUG");
                Debug.WriteLine(JsonSerializer.Serialize(value));
                Debug.WriteLine(JsonSerializer.Serialize(_validationErrors));
                #endif

                BtnAgregar.Enabled = !_validationErrors.Values.Any(
                    (error) => error.Trim() != string.Empty
                );

            });

        }

        private void InitListsViews()
        {
            LvwCoverturas.Items.Clear();
            foreach (Coverage coverage in Coverage.Coverages)
            {
                var item = new ListViewItem(coverage.Name);
                item.Checked = false;
                item.SubItems.Add(coverage.Detail);
                item.SubItems.Add(coverage.Percentage.ToString("0.00"));
                LvwCoverturas.Items.Add(item);
            }

            LvwAdicciones.Items.Clear();
            var addictions = Enum.GetNames(typeof(Addiction));
            foreach (var addiction in addictions)
            {
                var item = new ListViewItem(addiction);
                item.Checked = false;
                LvwAdicciones.Items.Add(item);
            }
        }

        #region ComponentsEvents
        private void DpkFechaNacimiento_ValueChanged(object sender, EventArgs e)
            => _clientBirthday.OnNext(DpkFechaNacimiento.Value);

        private void TxtCedula_TextChanged(object sender, EventArgs e)
            => _clientIdCard.OnNext(TxtCedula.Text.Replace(" ", "").Replace("-", ""));

        private void TxtNombre_TextChanged(object sender, EventArgs e)
            => _clientName.OnNext(Regex.Replace(TxtNombre.Text.Trim(), @"\s+", " "));

        private void CbxProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedValue = CbxProvincia.SelectedItem?.ToString();
            _clientProvince.OnNext(Provinces.Find(selectedValue));
        }

        private void CbxCanton_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedValue = CbxCanton.SelectedItem.ToString();
            _clientCanton.OnNext(_clientProvince.Value.Find(selectedValue));
        }

        private void CbxDristrito_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedValue = CbxDristrito.SelectedItem.ToString();
            _clientDistrict.OnNext(_clientCanton.Value.Find(selectedValue));
        }

        private void TxtSennas_TextChanged(object sender, EventArgs e)
            => _clientOtherSings.OnNext(TxtSennas.Text);

        private void RdbMasculino_CheckedChanged(object sender, EventArgs e)
        {
            if (!RdbMasculino.Checked) return;
            _clientGender.OnNext(Gender.Male);
        }

        private void RdbFemenino_CheckedChanged(object sender, EventArgs e)
        {
            if (!RdbFemenino.Checked) return;
            _clientGender.OnNext(Gender.Female);
        }

        private void CkbSortCanton_CheckedChanged(object sender, EventArgs e)
        {
            var province = _clientProvince.Value;
            var canton = _clientCanton.Value;
            ReloadCantons(province);
            CbxCanton.SelectedItem = canton?.Name;
        }

        private void CkbSortDistrito_CheckedChanged(object sender, EventArgs e)
        {
            var canton = _clientCanton.Value;
            var district = _clientDistrict.Value;
            ReloadDistricts(canton);
            CbxDristrito.SelectedItem = district?.Name;
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Estas a punto de limpiar el Formulario, ¿Continuar?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                ClearFormData();
            }
        } 

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            TblClients.DataSource = Clients.AppendAndSave(_formClient.Value);
            ClearFormData();
            ClearErrorDictionary();
        }

        private void TblClients_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (TblClients.SelectedRows.Count <= 0) return;
            var idCard = TblClients.SelectedRows[0].Cells["IdCard"].Value as string;
            CurrentClient = Clients.Data.FirstOrDefault(c => c.IdCard == idCard);
            FrameTabControl.TabPages.Add(RequestTab);
            FrameTabControl.SelectedIndex = 1;

            RegTxtCedula.Text = CurrentClient.IdCard;
            RegTxtNombre.Text = CurrentClient.Name;
            RegTxtEdad.Text = CurrentClient.Age.ToString();
            RegTxtGenero.Text = (CurrentClient.Gender == Gender.Male) ? "Masculino" : "Femenino";
            RefreshRequestTab();
        }

        private void LvwCoverturas_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in LvwCoverturas.Items)
            {
                var coverage = Coverage.Coverages.First((c) => c.Name == item.Text);
                if (item.Checked) _coverages.Add(coverage);
                else _coverages.Remove(coverage);
            }
            RefreshRequestTab();
        }

        private void LvwAdicciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            var adictions = Enum.GetNames(typeof(Addiction)).Select(a => a.ToLower()).ToArray();
            foreach (ListViewItem item in LvwAdicciones.Items)
            {
                Addiction? addiction = null;
                foreach (Addiction record in Enum.GetValues(typeof(Addiction)))
                {
                    var addictionName = Enum.GetName(typeof(Addiction), record).ToLower();
                    if (addictionName != item.Text.Replace(" ", "").ToLower()) continue;
                    addiction = record;
                    break;
                }
                if (addiction == null) return;
                if (item.Checked) _addictions.Add((Addiction)addiction);
                else _addictions.Remove((Addiction)addiction);
            }
            RefreshRequestTab();
        }

        private void BtnCalcular_Click(object sender, EventArgs e)
            => RefreshRequestTab();
        
        private void SpnMontoAsegurado_ValueChanged(object sender, EventArgs e)
            => RefreshRequestTab();
        #endregion

        #region CbxRelated
        private void ReloadCantons(SimpleTree value = null)
            => ReloadLocationCbx(CbxCanton, CkbSortCanton, _clientCanton, value);

        private void ReloadDistricts(SimpleTree value = null)
            => ReloadLocationCbx(CbxDristrito, CkbSortDistrito, _clientDistrict, value);

        private void ReloadLocationCbx(ComboBox comboBox, CheckBox checkBox, BehaviorSubject<SimpleTree> subject = null, SimpleTree value = null)
        {
            comboBox.Text = "";
            comboBox.Items.Clear();
            subject?.OnNext(null);
            comboBox.SelectedIndex = -1;
            comboBox.Enabled = !(value == null || value?.Childs == null);
            if (!comboBox.Enabled) return;
            var items = value.Childs.Select(p => p.Name);
            if (checkBox.Checked) items = items.OrderBy(s => s);
            comboBox.Items.AddRange(items.ToArray());
        }
        #endregion

        private void ClearFormData()
        {
            _loading.OnNext(true);
            TxtCedula.Text = string.Empty;
            TxtNombre.Text = string.Empty;
            DpkFechaNacimiento.MaxDate = DateTime.Now;
            RdbMasculino.Checked = true;
            CbxProvincia.SelectedIndex = -1;
            CbxProvincia.SelectedValue = "";
            CbxProvincia.Text = "";
            TxtSennas.Text = "";
            ClearErrorDictionary();
            _loading.OnNext(false);
            DpkFechaNacimiento.Value = DpkFechaNacimiento.MaxDate;
            TxtCedula.Focus();
        }

        private void ClearErrorDictionary()
        {
            var defaultValues = new Dictionary<string, string>
            {
                { "IdCard", "Required" },
                { "Birthday", "Required" },
                { "Name", "Required" },
                { "Province", "Required" },
                { "District", "Required" },
            };
            foreach (var key in _validationErrors.Keys.ToArray())
            {
                _validationErrors[key] = string.Empty;
            }
            foreach (var pair in defaultValues)
            {
                _validationErrors[pair.Key] = pair.Value;
            }
        }

        private void RefreshRequestTab()
        {
            var (coverages, insurance) = AmountCalculator.Calculate(
                CurrentClient,
                (float)SpnMontoAsegurado.Value,
                _coverages,
                _addictions
            );

            if (coverages.Count() > 0)
                TblResults.DataSource = new BindingSource
                {
                    DataSource = coverages.Select(c => new
                    {
                        Cobertura = c.Key,
                        Prima = String.Format("{0:#,##0.00}", c.Value),
                    })
                };
            else TblResults.DataSource = null;
            RegTxtPrima.Text = String.Format("{0:#,##0.00}", insurance);
        }

    }

}

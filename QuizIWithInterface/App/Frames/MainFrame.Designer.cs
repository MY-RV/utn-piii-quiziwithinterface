﻿namespace QuizIWithInterface
{
    partial class MainFrame
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrame));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.FrameTabControl = new System.Windows.Forms.TabControl();
            this.RegistrationTab = new System.Windows.Forms.TabPage();
            this.TblClients = new System.Windows.Forms.DataGridView();
            this.BtnAgregar = new System.Windows.Forms.Button();
            this.BtnNuevo = new System.Windows.Forms.Button();
            this.LblErrDistrito = new System.Windows.Forms.Label();
            this.LblErrCanton = new System.Windows.Forms.Label();
            this.LblErrProvincia = new System.Windows.Forms.Label();
            this.LblErrEdad = new System.Windows.Forms.Label();
            this.LblErrNombre = new System.Windows.Forms.Label();
            this.LblErrCedula = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CkbSortDistrito = new System.Windows.Forms.CheckBox();
            this.CbxDristrito = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CbxCanton = new System.Windows.Forms.ComboBox();
            this.CkbSortCanton = new System.Windows.Forms.CheckBox();
            this.DpkFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.RdbFemenino = new System.Windows.Forms.RadioButton();
            this.RdbMasculino = new System.Windows.Forms.RadioButton();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtEdad = new System.Windows.Forms.TextBox();
            this.CbxProvincia = new System.Windows.Forms.ComboBox();
            this.LblSennas = new System.Windows.Forms.Label();
            this.LblDistrito = new System.Windows.Forms.Label();
            this.LblCanton = new System.Windows.Forms.Label();
            this.LblProvincia = new System.Windows.Forms.Label();
            this.LblEdad = new System.Windows.Forms.Label();
            this.LblFechaNacimiento = new System.Windows.Forms.Label();
            this.LblGenero = new System.Windows.Forms.Label();
            this.TxtSennas = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.TxtCedula = new System.Windows.Forms.TextBox();
            this.LblCedula = new System.Windows.Forms.Label();
            this.RequestTab = new System.Windows.Forms.TabPage();
            this.RegLblPrima = new System.Windows.Forms.Label();
            this.RegTxtPrima = new System.Windows.Forms.TextBox();
            this.TblResults = new System.Windows.Forms.DataGridView();
            this.SpnMontoAsegurado = new System.Windows.Forms.NumericUpDown();
            this.LblMontoAsegurado = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LvwAdicciones = new System.Windows.Forms.ListView();
            this.HeaderAdiccion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.GrpCobertura = new System.Windows.Forms.GroupBox();
            this.LvwCoverturas = new System.Windows.Forms.ListView();
            this.HeaderCoverage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HeaderDetail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HeaderFee = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RegTxtGenero = new System.Windows.Forms.TextBox();
            this.RegTxtEdad = new System.Windows.Forms.TextBox();
            this.RegTxtNombre = new System.Windows.Forms.TextBox();
            this.RegTxtCedula = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnCalcular = new System.Windows.Forms.Button();
            this.FrameTabControl.SuspendLayout();
            this.RegistrationTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TblClients)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.RequestTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TblResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpnMontoAsegurado)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.GrpCobertura.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FrameTabControl
            // 
            resources.ApplyResources(this.FrameTabControl, "FrameTabControl");
            this.FrameTabControl.Controls.Add(this.RegistrationTab);
            this.FrameTabControl.Controls.Add(this.RequestTab);
            this.FrameTabControl.Name = "FrameTabControl";
            this.FrameTabControl.SelectedIndex = 0;
            // 
            // RegistrationTab
            // 
            resources.ApplyResources(this.RegistrationTab, "RegistrationTab");
            this.RegistrationTab.Controls.Add(this.TblClients);
            this.RegistrationTab.Controls.Add(this.BtnAgregar);
            this.RegistrationTab.Controls.Add(this.BtnNuevo);
            this.RegistrationTab.Controls.Add(this.LblErrDistrito);
            this.RegistrationTab.Controls.Add(this.LblErrCanton);
            this.RegistrationTab.Controls.Add(this.LblErrProvincia);
            this.RegistrationTab.Controls.Add(this.LblErrEdad);
            this.RegistrationTab.Controls.Add(this.LblErrNombre);
            this.RegistrationTab.Controls.Add(this.LblErrCedula);
            this.RegistrationTab.Controls.Add(this.panel2);
            this.RegistrationTab.Controls.Add(this.panel1);
            this.RegistrationTab.Controls.Add(this.DpkFechaNacimiento);
            this.RegistrationTab.Controls.Add(this.RdbFemenino);
            this.RegistrationTab.Controls.Add(this.RdbMasculino);
            this.RegistrationTab.Controls.Add(this.TxtNombre);
            this.RegistrationTab.Controls.Add(this.TxtEdad);
            this.RegistrationTab.Controls.Add(this.CbxProvincia);
            this.RegistrationTab.Controls.Add(this.LblSennas);
            this.RegistrationTab.Controls.Add(this.LblDistrito);
            this.RegistrationTab.Controls.Add(this.LblCanton);
            this.RegistrationTab.Controls.Add(this.LblProvincia);
            this.RegistrationTab.Controls.Add(this.LblEdad);
            this.RegistrationTab.Controls.Add(this.LblFechaNacimiento);
            this.RegistrationTab.Controls.Add(this.LblGenero);
            this.RegistrationTab.Controls.Add(this.TxtSennas);
            this.RegistrationTab.Controls.Add(this.lblNombre);
            this.RegistrationTab.Controls.Add(this.TxtCedula);
            this.RegistrationTab.Controls.Add(this.LblCedula);
            this.RegistrationTab.Name = "RegistrationTab";
            this.RegistrationTab.UseVisualStyleBackColor = true;
            // 
            // TblClients
            // 
            resources.ApplyResources(this.TblClients, "TblClients");
            this.TblClients.AllowUserToAddRows = false;
            this.TblClients.AllowUserToDeleteRows = false;
            this.TblClients.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TblClients.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.TblClients.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.TblClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TblClients.Name = "TblClients";
            this.TblClients.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TblClients.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TblClients.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.TblClients.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TblClients_CellDoubleClick);
            // 
            // BtnAgregar
            // 
            resources.ApplyResources(this.BtnAgregar, "BtnAgregar");
            this.BtnAgregar.BackColor = System.Drawing.Color.LightCyan;
            this.BtnAgregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnAgregar.Name = "BtnAgregar";
            this.BtnAgregar.UseVisualStyleBackColor = false;
            this.BtnAgregar.Click += new System.EventHandler(this.BtnAgregar_Click);
            // 
            // BtnNuevo
            // 
            resources.ApplyResources(this.BtnNuevo, "BtnNuevo");
            this.BtnNuevo.BackColor = System.Drawing.Color.LightCyan;
            this.BtnNuevo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnNuevo.Name = "BtnNuevo";
            this.BtnNuevo.UseVisualStyleBackColor = false;
            this.BtnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // LblErrDistrito
            // 
            resources.ApplyResources(this.LblErrDistrito, "LblErrDistrito");
            this.LblErrDistrito.ForeColor = System.Drawing.Color.Firebrick;
            this.LblErrDistrito.Name = "LblErrDistrito";
            // 
            // LblErrCanton
            // 
            resources.ApplyResources(this.LblErrCanton, "LblErrCanton");
            this.LblErrCanton.ForeColor = System.Drawing.Color.Firebrick;
            this.LblErrCanton.Name = "LblErrCanton";
            // 
            // LblErrProvincia
            // 
            resources.ApplyResources(this.LblErrProvincia, "LblErrProvincia");
            this.LblErrProvincia.ForeColor = System.Drawing.Color.Firebrick;
            this.LblErrProvincia.Name = "LblErrProvincia";
            // 
            // LblErrEdad
            // 
            resources.ApplyResources(this.LblErrEdad, "LblErrEdad");
            this.LblErrEdad.ForeColor = System.Drawing.Color.Firebrick;
            this.LblErrEdad.Name = "LblErrEdad";
            // 
            // LblErrNombre
            // 
            resources.ApplyResources(this.LblErrNombre, "LblErrNombre");
            this.LblErrNombre.ForeColor = System.Drawing.Color.Firebrick;
            this.LblErrNombre.Name = "LblErrNombre";
            // 
            // LblErrCedula
            // 
            resources.ApplyResources(this.LblErrCedula, "LblErrCedula");
            this.LblErrCedula.ForeColor = System.Drawing.Color.Firebrick;
            this.LblErrCedula.Name = "LblErrCedula";
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.CkbSortDistrito);
            this.panel2.Controls.Add(this.CbxDristrito);
            this.panel2.Name = "panel2";
            // 
            // CkbSortDistrito
            // 
            resources.ApplyResources(this.CkbSortDistrito, "CkbSortDistrito");
            this.CkbSortDistrito.Checked = true;
            this.CkbSortDistrito.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CkbSortDistrito.Name = "CkbSortDistrito";
            this.CkbSortDistrito.UseVisualStyleBackColor = true;
            this.CkbSortDistrito.CheckedChanged += new System.EventHandler(this.CkbSortDistrito_CheckedChanged);
            // 
            // CbxDristrito
            // 
            resources.ApplyResources(this.CbxDristrito, "CbxDristrito");
            this.CbxDristrito.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CbxDristrito.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CbxDristrito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxDristrito.FormattingEnabled = true;
            this.CbxDristrito.Name = "CbxDristrito";
            this.CbxDristrito.SelectedIndexChanged += new System.EventHandler(this.CbxDristrito_SelectedIndexChanged);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.CbxCanton);
            this.panel1.Controls.Add(this.CkbSortCanton);
            this.panel1.Name = "panel1";
            // 
            // CbxCanton
            // 
            resources.ApplyResources(this.CbxCanton, "CbxCanton");
            this.CbxCanton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CbxCanton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CbxCanton.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxCanton.FormattingEnabled = true;
            this.CbxCanton.Name = "CbxCanton";
            this.CbxCanton.SelectedIndexChanged += new System.EventHandler(this.CbxCanton_SelectedIndexChanged);
            // 
            // CkbSortCanton
            // 
            resources.ApplyResources(this.CkbSortCanton, "CkbSortCanton");
            this.CkbSortCanton.Checked = true;
            this.CkbSortCanton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CkbSortCanton.Name = "CkbSortCanton";
            this.CkbSortCanton.UseVisualStyleBackColor = true;
            this.CkbSortCanton.CheckedChanged += new System.EventHandler(this.CkbSortCanton_CheckedChanged);
            // 
            // DpkFechaNacimiento
            // 
            resources.ApplyResources(this.DpkFechaNacimiento, "DpkFechaNacimiento");
            this.DpkFechaNacimiento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DpkFechaNacimiento.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DpkFechaNacimiento.Name = "DpkFechaNacimiento";
            this.DpkFechaNacimiento.Value = new System.DateTime(2023, 6, 17, 0, 0, 0, 0);
            this.DpkFechaNacimiento.ValueChanged += new System.EventHandler(this.DpkFechaNacimiento_ValueChanged);
            // 
            // RdbFemenino
            // 
            resources.ApplyResources(this.RdbFemenino, "RdbFemenino");
            this.RdbFemenino.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RdbFemenino.Name = "RdbFemenino";
            this.RdbFemenino.TabStop = true;
            this.RdbFemenino.UseVisualStyleBackColor = true;
            this.RdbFemenino.CheckedChanged += new System.EventHandler(this.RdbFemenino_CheckedChanged);
            // 
            // RdbMasculino
            // 
            resources.ApplyResources(this.RdbMasculino, "RdbMasculino");
            this.RdbMasculino.Checked = true;
            this.RdbMasculino.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RdbMasculino.Name = "RdbMasculino";
            this.RdbMasculino.TabStop = true;
            this.RdbMasculino.UseVisualStyleBackColor = true;
            this.RdbMasculino.CheckedChanged += new System.EventHandler(this.RdbMasculino_CheckedChanged);
            // 
            // TxtNombre
            // 
            resources.ApplyResources(this.TxtNombre, "TxtNombre");
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.TextChanged += new System.EventHandler(this.TxtNombre_TextChanged);
            // 
            // TxtEdad
            // 
            resources.ApplyResources(this.TxtEdad, "TxtEdad");
            this.TxtEdad.Name = "TxtEdad";
            this.TxtEdad.ReadOnly = true;
            // 
            // CbxProvincia
            // 
            resources.ApplyResources(this.CbxProvincia, "CbxProvincia");
            this.CbxProvincia.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CbxProvincia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CbxProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxProvincia.FormattingEnabled = true;
            this.CbxProvincia.Name = "CbxProvincia";
            this.CbxProvincia.SelectedIndexChanged += new System.EventHandler(this.CbxProvincia_SelectedIndexChanged);
            // 
            // LblSennas
            // 
            resources.ApplyResources(this.LblSennas, "LblSennas");
            this.LblSennas.Name = "LblSennas";
            // 
            // LblDistrito
            // 
            resources.ApplyResources(this.LblDistrito, "LblDistrito");
            this.LblDistrito.Name = "LblDistrito";
            // 
            // LblCanton
            // 
            resources.ApplyResources(this.LblCanton, "LblCanton");
            this.LblCanton.Name = "LblCanton";
            // 
            // LblProvincia
            // 
            resources.ApplyResources(this.LblProvincia, "LblProvincia");
            this.LblProvincia.Name = "LblProvincia";
            // 
            // LblEdad
            // 
            resources.ApplyResources(this.LblEdad, "LblEdad");
            this.LblEdad.Name = "LblEdad";
            // 
            // LblFechaNacimiento
            // 
            resources.ApplyResources(this.LblFechaNacimiento, "LblFechaNacimiento");
            this.LblFechaNacimiento.Name = "LblFechaNacimiento";
            // 
            // LblGenero
            // 
            resources.ApplyResources(this.LblGenero, "LblGenero");
            this.LblGenero.Name = "LblGenero";
            // 
            // TxtSennas
            // 
            resources.ApplyResources(this.TxtSennas, "TxtSennas");
            this.TxtSennas.Name = "TxtSennas";
            this.TxtSennas.TextChanged += new System.EventHandler(this.TxtSennas_TextChanged);
            // 
            // lblNombre
            // 
            resources.ApplyResources(this.lblNombre, "lblNombre");
            this.lblNombre.Name = "lblNombre";
            // 
            // TxtCedula
            // 
            resources.ApplyResources(this.TxtCedula, "TxtCedula");
            this.TxtCedula.Name = "TxtCedula";
            this.TxtCedula.TextChanged += new System.EventHandler(this.TxtCedula_TextChanged);
            // 
            // LblCedula
            // 
            resources.ApplyResources(this.LblCedula, "LblCedula");
            this.LblCedula.Name = "LblCedula";
            // 
            // RequestTab
            // 
            resources.ApplyResources(this.RequestTab, "RequestTab");
            this.RequestTab.Controls.Add(this.RegLblPrima);
            this.RequestTab.Controls.Add(this.RegTxtPrima);
            this.RequestTab.Controls.Add(this.TblResults);
            this.RequestTab.Controls.Add(this.SpnMontoAsegurado);
            this.RequestTab.Controls.Add(this.LblMontoAsegurado);
            this.RequestTab.Controls.Add(this.groupBox3);
            this.RequestTab.Controls.Add(this.GrpCobertura);
            this.RequestTab.Controls.Add(this.groupBox1);
            this.RequestTab.Controls.Add(this.BtnCalcular);
            this.RequestTab.Name = "RequestTab";
            this.RequestTab.UseVisualStyleBackColor = true;
            // 
            // RegLblPrima
            // 
            resources.ApplyResources(this.RegLblPrima, "RegLblPrima");
            this.RegLblPrima.Name = "RegLblPrima";
            // 
            // RegTxtPrima
            // 
            resources.ApplyResources(this.RegTxtPrima, "RegTxtPrima");
            this.RegTxtPrima.Name = "RegTxtPrima";
            this.RegTxtPrima.ReadOnly = true;
            // 
            // TblResults
            // 
            resources.ApplyResources(this.TblResults, "TblResults");
            this.TblResults.AllowUserToAddRows = false;
            this.TblResults.AllowUserToDeleteRows = false;
            this.TblResults.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TblResults.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.TblResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.TblResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TblResults.Name = "TblResults";
            this.TblResults.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TblResults.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TblResults.RowsDefaultCellStyle = dataGridViewCellStyle6;
            // 
            // SpnMontoAsegurado
            // 
            resources.ApplyResources(this.SpnMontoAsegurado, "SpnMontoAsegurado");
            this.SpnMontoAsegurado.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.SpnMontoAsegurado.Minimum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.SpnMontoAsegurado.Name = "SpnMontoAsegurado";
            this.SpnMontoAsegurado.Value = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.SpnMontoAsegurado.ValueChanged += new System.EventHandler(this.SpnMontoAsegurado_ValueChanged);
            // 
            // LblMontoAsegurado
            // 
            resources.ApplyResources(this.LblMontoAsegurado, "LblMontoAsegurado");
            this.LblMontoAsegurado.Name = "LblMontoAsegurado";
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.LvwAdicciones);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // LvwAdicciones
            // 
            resources.ApplyResources(this.LvwAdicciones, "LvwAdicciones");
            this.LvwAdicciones.CheckBoxes = true;
            this.LvwAdicciones.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HeaderAdiccion});
            this.LvwAdicciones.FullRowSelect = true;
            this.LvwAdicciones.GridLines = true;
            this.LvwAdicciones.HideSelection = false;
            this.LvwAdicciones.Name = "LvwAdicciones";
            this.LvwAdicciones.UseCompatibleStateImageBehavior = false;
            this.LvwAdicciones.View = System.Windows.Forms.View.List;
            this.LvwAdicciones.SelectedIndexChanged += new System.EventHandler(this.LvwAdicciones_SelectedIndexChanged);
            // 
            // HeaderAdiccion
            // 
            resources.ApplyResources(this.HeaderAdiccion, "HeaderAdiccion");
            // 
            // GrpCobertura
            // 
            resources.ApplyResources(this.GrpCobertura, "GrpCobertura");
            this.GrpCobertura.Controls.Add(this.LvwCoverturas);
            this.GrpCobertura.ForeColor = System.Drawing.SystemColors.Highlight;
            this.GrpCobertura.Name = "GrpCobertura";
            this.GrpCobertura.TabStop = false;
            // 
            // LvwCoverturas
            // 
            resources.ApplyResources(this.LvwCoverturas, "LvwCoverturas");
            this.LvwCoverturas.CheckBoxes = true;
            this.LvwCoverturas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HeaderCoverage,
            this.HeaderDetail,
            this.HeaderFee});
            this.LvwCoverturas.FullRowSelect = true;
            this.LvwCoverturas.GridLines = true;
            this.LvwCoverturas.HideSelection = false;
            this.LvwCoverturas.Name = "LvwCoverturas";
            this.LvwCoverturas.UseCompatibleStateImageBehavior = false;
            this.LvwCoverturas.View = System.Windows.Forms.View.Details;
            this.LvwCoverturas.SelectedIndexChanged += new System.EventHandler(this.LvwCoverturas_SelectedIndexChanged);
            // 
            // HeaderCoverage
            // 
            resources.ApplyResources(this.HeaderCoverage, "HeaderCoverage");
            // 
            // HeaderDetail
            // 
            resources.ApplyResources(this.HeaderDetail, "HeaderDetail");
            // 
            // HeaderFee
            // 
            resources.ApplyResources(this.HeaderFee, "HeaderFee");
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.RegTxtGenero);
            this.groupBox1.Controls.Add(this.RegTxtEdad);
            this.groupBox1.Controls.Add(this.RegTxtNombre);
            this.groupBox1.Controls.Add(this.RegTxtCedula);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // RegTxtGenero
            // 
            resources.ApplyResources(this.RegTxtGenero, "RegTxtGenero");
            this.RegTxtGenero.Name = "RegTxtGenero";
            this.RegTxtGenero.ReadOnly = true;
            // 
            // RegTxtEdad
            // 
            resources.ApplyResources(this.RegTxtEdad, "RegTxtEdad");
            this.RegTxtEdad.Name = "RegTxtEdad";
            this.RegTxtEdad.ReadOnly = true;
            // 
            // RegTxtNombre
            // 
            resources.ApplyResources(this.RegTxtNombre, "RegTxtNombre");
            this.RegTxtNombre.Name = "RegTxtNombre";
            this.RegTxtNombre.ReadOnly = true;
            // 
            // RegTxtCedula
            // 
            resources.ApplyResources(this.RegTxtCedula, "RegTxtCedula");
            this.RegTxtCedula.Name = "RegTxtCedula";
            this.RegTxtCedula.ReadOnly = true;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            // 
            // BtnCalcular
            // 
            resources.ApplyResources(this.BtnCalcular, "BtnCalcular");
            this.BtnCalcular.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnCalcular.Image = global::QuizIWithInterface.Properties.Resources.refresh;
            this.BtnCalcular.Name = "BtnCalcular";
            this.BtnCalcular.UseVisualStyleBackColor = true;
            this.BtnCalcular.Click += new System.EventHandler(this.BtnCalcular_Click);
            // 
            // MainFrame
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.FrameTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainFrame";
            this.FrameTabControl.ResumeLayout(false);
            this.RegistrationTab.ResumeLayout(false);
            this.RegistrationTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TblClients)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.RequestTab.ResumeLayout(false);
            this.RequestTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TblResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpnMontoAsegurado)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.GrpCobertura.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl FrameTabControl;
        private System.Windows.Forms.TabPage RegistrationTab;
        private System.Windows.Forms.TabPage RequestTab;
        private System.Windows.Forms.Label LblProvincia;
        private System.Windows.Forms.Label LblEdad;
        private System.Windows.Forms.Label LblFechaNacimiento;
        private System.Windows.Forms.Label LblGenero;
        private System.Windows.Forms.TextBox TxtSennas;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox TxtCedula;
        private System.Windows.Forms.Label LblCedula;
        private System.Windows.Forms.Label LblSennas;
        private System.Windows.Forms.Label LblDistrito;
        private System.Windows.Forms.Label LblCanton;
        private System.Windows.Forms.TextBox TxtEdad;
        private System.Windows.Forms.ComboBox CbxProvincia;
        private System.Windows.Forms.ComboBox CbxCanton;
        private System.Windows.Forms.ComboBox CbxDristrito;
        private System.Windows.Forms.DateTimePicker DpkFechaNacimiento;
        private System.Windows.Forms.RadioButton RdbFemenino;
        private System.Windows.Forms.RadioButton RdbMasculino;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.CheckBox CkbSortCanton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox CkbSortDistrito;
        private System.Windows.Forms.Label LblErrDistrito;
        private System.Windows.Forms.Label LblErrCanton;
        private System.Windows.Forms.Label LblErrProvincia;
        private System.Windows.Forms.Label LblErrEdad;
        private System.Windows.Forms.Label LblErrNombre;
        private System.Windows.Forms.Label LblErrCedula;
        private System.Windows.Forms.Button BtnAgregar;
        private System.Windows.Forms.Button BtnNuevo;
        private System.Windows.Forms.DataGridView TblClients;
        private System.Windows.Forms.GroupBox GrpCobertura;
        private System.Windows.Forms.ListView LvwCoverturas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox RegTxtGenero;
        private System.Windows.Forms.TextBox RegTxtEdad;
        private System.Windows.Forms.TextBox RegTxtNombre;
        private System.Windows.Forms.TextBox RegTxtCedula;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader HeaderCoverage;
        private System.Windows.Forms.ColumnHeader HeaderDetail;
        private System.Windows.Forms.ColumnHeader HeaderFee;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView LvwAdicciones;
        private System.Windows.Forms.ColumnHeader HeaderAdiccion;
        private System.Windows.Forms.Button BtnCalcular;
        private System.Windows.Forms.NumericUpDown SpnMontoAsegurado;
        private System.Windows.Forms.Label LblMontoAsegurado;
        private System.Windows.Forms.DataGridView TblResults;
        private System.Windows.Forms.Label RegLblPrima;
        private System.Windows.Forms.TextBox RegTxtPrima;
    }
}


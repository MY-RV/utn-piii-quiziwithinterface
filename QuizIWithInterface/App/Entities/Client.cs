﻿using QuizIWithInterface.MYRV.Extensions;
using QuizIWithInterface.App.Lib.Enums;
using System;
using System.ComponentModel;

namespace QuizIWithInterface.App.Entities
{
    internal class Client
    {
        [DisplayName("Identificación")]
        public string IdCard { get; set; } = string.Empty;

        [DisplayName("Nombre")]
        public string Name { get; set; } = string.Empty;

        [DisplayName("Genero")]
        public Gender? Gender { get; set; } = null;

        [DisplayName("Nacido el")]
        public DateTime? Birthday { get; set; } = null;

        [DisplayName("Provincia")]
        public string Province { get; set; } = string.Empty;

        [DisplayName("Cantón")]
        public string Canton { get; set; } = string.Empty;

        [DisplayName("Distrito")]
        public string District { get; set; } = string.Empty;

        [DisplayName("Otras Señas")]
        public string OtherSigns { get; set; } = string.Empty;

        [DisplayName("Edad")]
        public int? Age { get => Birthday?.GetRoundedAge(); }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizIWithInterface.App.Lib.Datasets
{
    internal class Coverage
    {
        public string Name { get; } = string.Empty;
        public string Detail { get; } = string.Empty;
        public float Percentage { get; }

        private Coverage(string Name, string Detail, float Percentage)
        {
            this.Name = Name;
            this.Detail = Detail;
            this.Percentage = Percentage;
        }

        public static readonly Coverage TypeA = new Coverage("A", "Beneficio Principal", 2.50f);
        public static readonly Coverage TypeB = new Coverage("B", "Beneficio del DID", 0.30f);
        public static readonly Coverage TypeC = new Coverage("C", "Beneficio del AMSA", 0.35f);
        public static readonly Coverage TypeD = new Coverage("D", "Servicios Funerarios", 0.10f);

        public static readonly IEnumerable<Coverage> Coverages = new[] { TypeA, TypeB, TypeC, TypeD };
    }
}

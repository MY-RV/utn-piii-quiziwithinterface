﻿using QuizIWithInterface.App.Entities;
using System.Collections.Generic;
using System.Text.Json;
using System.Linq;
using System.IO;
using System;

namespace QuizIWithInterface.App.Lib.Datasets
{
    internal static class Clients
    {
        private static IEnumerable<Client> _data;
        private const string FILE_PATH = "Datasets/Clients.json";
        private static readonly JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions
        {
            Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
            WriteIndented = true
        };

        public static IEnumerable<Client> Data { get => _data ?? RefreshedData; }
        public static IEnumerable<Client> RefreshedData { get => _data = ReadData(); }

        public static IEnumerable<Client> AppendAndSave(Client client)
        {
            var content = JsonSerializer.Serialize(Data.Append(client), jsonSerializerOptions);
            File.WriteAllText(FILE_PATH, content);
            return RefreshedData;
        }

        private static IEnumerable<Client> ReadData()
        {
            try
            {
                using (StreamReader sr = new StreamReader(FILE_PATH))
                {
                    return JsonSerializer.Deserialize<IEnumerable<Client>>(sr.ReadToEnd());
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File Not Found.");
            }
            catch (IOException ex)
            {
                Console.WriteLine($"Reading Error: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Reading Error: {ex.Message}");
            }
            return Enumerable.Empty<Client>();
        }

    }
}

﻿using QuizIWithInterface.App.Lib.Types;
using System.Collections.Generic;
using System.Text.Json;
using System.IO;
using System;

namespace QuizIWithInterface.App.Lib.Datasets
{
    internal static class Provinces
    {

        private static IEnumerable<SimpleTree> _data;
        private const string FILE_PATH = "Datasets/CR_Provinces.json";

        public static IEnumerable<SimpleTree> Data { get => _data ?? RefreshedData; }
        public static IEnumerable<SimpleTree> RefreshedData { get => _data = ReadData(); }

        public static SimpleTree Find(string name, int level = 0) 
        {
            if (level > 2 || level < 0) return null;
            foreach (var child in Data)
            {
                if (level == 0 && child.Name == name) return child;
                var result = child.Find(name, level - 1);
                if (result != null) return result;
            }
            return null;
        }

        private static IEnumerable<SimpleTree> ReadData() 
        {
            try
            {
                using (StreamReader sr = new StreamReader(FILE_PATH))
                {
                    return JsonSerializer.Deserialize<IEnumerable<SimpleTree>>(sr.ReadToEnd());
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File Not Found.");
            }
            catch (IOException ex)
            {
                Console.WriteLine($"Reading Error: {ex.Message}");
            }
            return new SimpleTree[] { };
        }

    }

}

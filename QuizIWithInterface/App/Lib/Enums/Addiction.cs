﻿namespace QuizIWithInterface.App.Lib.Enums
{
    internal enum Addiction
    {
        Licor,
        Fumado,
        Marihuana,
        Cocaina,
        OtroTipoDeDrogas,
    }
}

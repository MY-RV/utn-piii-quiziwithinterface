﻿using QuizIWithInterface.App.Lib.Datasets;
using QuizIWithInterface.App.Lib.Enums;
using QuizIWithInterface.App.Entities;
using System.Collections.Generic;
using System.Linq;
using System;

namespace QuizIWithInterface.App.Lib
{
    internal static class AmountCalculator
    {
        public static (Dictionary<string, float>, double) Calculate(
            Client client, float amount,
            IEnumerable<Coverage> coverages,
            IEnumerable<Addiction> addictions = null)
        {
            if (client.Age < 18) throw new Exception("You're to young");
            if (client.Age > 50) throw new Exception("You're to old");
            if (
                amount > 100000000
                || amount < 3000000
            ) throw new Exception("The amount should be between 3,000,000.00 and 100,000,000.00");
            if (client.Gender == Gender.Male) amount *= 1.05f;

            if (addictions?.Count() == 1)
                amount *= 1.05f;
            else if (addictions?.Count() <= 3 && addictions?.Count() > 1)
                amount *= 1.10f;
            else if (addictions?.Count() > 3)
                amount *= 1.15f;
            var dic = new Dictionary<string, float>();
            var sum = 0d;

            foreach (var coverage in coverages)
            {
                amount *= (1 + (coverage.Percentage / 100));
                dic[coverage.Name] = amount;
                sum += amount;
            }

            if (client.Age > 40) sum *= 1.25f;
            return (dic, sum);
        }
    }
}

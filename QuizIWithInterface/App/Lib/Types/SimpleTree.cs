﻿using System.Collections.Generic;

namespace QuizIWithInterface.App.Lib.Types
{

    internal  class SimpleTree 
    {
        public string Name { get; set; }
        public IEnumerable<SimpleTree> Childs { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public SimpleTree Find(string name, int level = 0)
        {
            if (level < 0) return null;
            if (Childs == null) return null;
            foreach (var child in Childs)
            {
                if (level == 0 && child.Name == name) return child;
                var result = child.Find(name, level - 1);
                if (result != null) return result;
            }
            return null;
        }

    }

}

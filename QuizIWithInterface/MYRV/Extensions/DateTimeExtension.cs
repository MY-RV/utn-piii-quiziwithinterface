﻿using System;

namespace QuizIWithInterface.MYRV.Extensions
{

    internal static class DateTimeExtension
    {

        public static int GetRoundedAge(this DateTime birthdate)
        {
            var years = DateTime.Now.Year - birthdate.Year;
            var months = DateTime.Now.Month - birthdate.Month;
            if (months >= 6) years++;
            return years;
        }

    }

}

﻿using Autofac;
using System;

namespace QuizIWithInterface.MYRV.DependencyInjection
{

    internal static partial class Dependency
    {
        public static IContainer Container { get; private set; }

        public static IContainer BuildContainer(Action<ContainerBuilder> BuilderAction)
        {
            var containerBuilder = new ContainerBuilder();
            BuilderAction(containerBuilder);
            Container = containerBuilder.Build();
            return Container;
        }

    }

}
